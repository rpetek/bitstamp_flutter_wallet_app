import 'package:bitstamp_flutter_wallet_app/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class AppTextStyles {

    static const TextStyle textLarge = TextStyle(
        fontSize: 28, color: AppColors.primaryTextColor
    );

    static const TextStyle textSuccess = TextStyle(
        fontSize: 24,
        color: AppColors.primaryTextColor,
        fontWeight: FontWeight.w500
    );

    static const TextStyle textPin = TextStyle(
        fontSize: 22, color: AppColors.primaryTextColor
    );

    static const TextStyle textBig = TextStyle(
        fontSize: 18, color: AppColors.primaryTextColor
    );

    static const TextStyle textNormal = TextStyle(
        fontSize: 16, color: AppColors.primaryTextColor
    );

    static const TextStyle textSmall = TextStyle(
        fontSize: 14, color: AppColors.secondaryTextColor
    );

    static const TextStyle textSmallError = TextStyle(
        fontSize: 12, color: AppColors.red
    );

    static const TextStyle textItemTitle = TextStyle(
        fontSize: 16, color: AppColors.primaryTextColor
    );

    static const TextStyle textItemSmallTitle = TextStyle(
        fontSize: 12,
        color: AppColors.primaryTextColor,
        fontWeight: FontWeight.w400
    );

    static const TextStyle textItemSubtitle = TextStyle(
        fontSize: 14, color: AppColors.secondaryTextColor
    );

    static const TextStyle textButton = TextStyle(
        fontSize: 14, color: AppColors.white
    );

    static const TextStyle textButtonSmall = TextStyle(
        fontSize: 14, color: AppColors.white
    );

    static const TextStyle listHeader = TextStyle(
        fontSize: 12, color: AppColors.gray
    );

    static const TextStyle listItemContactTitle = TextStyle(
        fontSize: 14, color: AppColors.primaryTextColor
    );

    static const TextStyle listItemContactSubtitle = TextStyle(
        fontSize: 12, color: AppColors.gray
    );

}

class AppPadding {

    static const double paddingQuad = 64;
    static const double paddingTriple = 48;
    static const double paddingDouble = 32;
    static const double paddingBig = 24;
    static const double padding = 16;
    static const double paddingSmall = 12;
    static const double paddingMini = 8;
    static const double padding6 = 6;
    static const double padding4 = 4;
    static const double padding2 = 2;

}