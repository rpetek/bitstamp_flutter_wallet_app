import 'package:bitstamp_flutter_wallet_app/colors.dart';
import 'package:bitstamp_flutter_wallet_app/resources/assets.dart';
import 'package:bitstamp_flutter_wallet_app/resources/screens.dart';
import 'package:bitstamp_flutter_wallet_app/resources/strings.dart';
import 'package:bitstamp_flutter_wallet_app/screens/card/card_screen.dart';
import 'package:bitstamp_flutter_wallet_app/screens/contacts/contacts_screen.dart';
import 'package:bitstamp_flutter_wallet_app/screens/settings/settings_screen.dart';
import 'package:bitstamp_flutter_wallet_app/screens/wallet/wallet_screen.dart';
import 'package:bitstamp_flutter_wallet_app/styles.dart';
import 'package:bitstamp_flutter_wallet_app/widgets/bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';

import 'enums/screens.dart';

void main() => runApp(BitstampApp());

class BitstampApp extends StatelessWidget {
    // This widget is the root of your application.
    @override
    Widget build(BuildContext context) {
        SystemChrome.setPreferredOrientations([
            DeviceOrientation.portraitUp
        ]);
        return MaterialApp(
            title: 'Bitstamp card app',
            theme: ThemeData(
                primarySwatch: Colors.grey,
            ),
            home: Scaffold(
                body: MainScreen(title: 'Card')
            )
        );
    }
}

class MainScreen extends StatefulWidget {
    MainScreen({Key key, this.title}) : super(key: key);

    // Fields in a Widget subclass are always marked "final".
    String title;

    @override
    _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {

    Screens currentScreen = Screens.OVERVIEW;

    final List<Widget> pages = [
        CardScreen(
            key: PageStorageKey(Screens.OVERVIEW),
        ),
        WalletScreen(
            key: PageStorageKey(Screens.SEND),
        ),
        SettingsScreen(
            key: PageStorageKey(Screens.SETTINGS),
        ),
    ];
    final PageStorageBucket bucket = PageStorageBucket();
    final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

    @override
    Widget build(BuildContext context) {
        return Container(
            child: Scaffold(
                backgroundColor: AppColors.backgroundColor,
                appBar: AppBar(
                    backgroundColor: AppColors.backgroundColor,
                    textTheme: TextTheme(
                        title: TextStyle(
                            fontSize: 18,
                            color: AppColors.primaryTextColor
                        )
                    ),
                    elevation: 0,
                    title: Text(widget.title),
                ),
                body: PageStorage(
                    key: PageStorageKey(currentScreen),
                    child: pages[currentScreen.value],
                    bucket: bucket,
                ),
                bottomNavigationBar: BottomBar(
                    color: AppColors.secondaryTextColor,
                    colorActive: AppColors.primaryTextColor,
                    onTabChange: ((Screens screen) {
                        print("tab changed:$screen");
                        if (screen == Screens.SEND) {
                            showModalBottomSheet(
                                context: context,
                                backgroundColor: AppColors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(15.0),
                                        topRight: Radius.circular(15.0)),
                                ),
                                builder: (context) => ActionSheet()
                            );
                        } else {
                            setState(() {
                                if (screen == Screens.OVERVIEW) {
                                    widget.title = "Card";
                                } else if (screen == Screens.SETTINGS) {
                                    widget.title = "Settings";
                                }
                                currentScreen = screen;
                            });
                        }
                    })
                )
            )
        );
    }
}

enum ActionType {
    REQUEST,
    SEND,
    TRANSFER
}

class ActionSheet extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return Container(
            child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                    ActionSheetRow(
                        type: ActionType.REQUEST,
                        iconAsset: Assets.ic_action_request,
                        title: Strings.request_title,
                        subtitle: Strings.request_subtitle,
                        onTap: () {
                            Navigator.pop(context);
                            Navigator.of(context).push(
                                MaterialPageRoute<Null>(
                                    settings: RouteSettings(
                                        name: AppScreens.screen_contacts),
                                    builder: (BuildContext context) {
                                        return ContactsScreen(
                                            actionType: ActionType.REQUEST);
                                    }
                                ));
                        }
                    ),
                    ActionSheetRowDivider(),
                    ActionSheetRow(
                        type: ActionType.SEND,
                        iconAsset: Assets.ic_action_send,
                        title: Strings.send_title,
                        subtitle: Strings.send_subtitle,
                        onTap: () {
                            Navigator.pop(context);
                            Navigator.of(context).push(
                                MaterialPageRoute<Null>(
                                    settings: RouteSettings(
                                        name: AppScreens.screen_contacts),
                                    builder: (BuildContext context) {
                                        return ContactsScreen(
                                            actionType: ActionType.SEND);
                                    }
                                ));
                        }),
                    ActionSheetRowDivider(),
                    ActionSheetRow(
                        type: ActionType.TRANSFER,
                        iconAsset: Assets.ic_action_transfer,
                        title: Strings.transfer_title,
                        subtitle: Strings.transfer_subtitle,
                        onTap: () {
                            Navigator.pop(context);
                            Navigator.of(context).push(
                                MaterialPageRoute<Null>(
                                    settings: RouteSettings(
                                        name: AppScreens.screen_contacts),
                                    builder: (BuildContext context) {
                                        return ContactsScreen(
                                            actionType: ActionType.TRANSFER);
                                    }
                                ));
                        }),
                ]
            )
        );
    }
}

class ActionSheetRow extends StatelessWidget {
    final ActionType type;
    final String iconAsset;
    final String title;
    final String subtitle;
    final Function onTap;

    const ActionSheetRow({
        @required this.type,
        @required this.iconAsset,
        @required this.title,
        @required this.subtitle,
        this.onTap}) :super();

    @override
    Widget build(BuildContext context) {
        return Container(
            height: 100,
            child: InkWell(
                onTap: () {
                    this.onTap();
                },
                child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                        Padding(
                            padding: EdgeInsets.all(AppPadding.padding),
                            child: SvgPicture.asset(iconAsset),
                        ),
                        Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                                Text(title,
                                    style: AppTextStyles.textItemTitle),
                                Text(subtitle,
                                    style: AppTextStyles.textItemSubtitle)
                            ],
                        )
                    ],
                )
            )
        );
    }
}

class ActionSheetRowDivider extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return Container(
            height: 1,
            color: AppColors.grayLight,
        );
    }
}
