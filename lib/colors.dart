import 'dart:ui';

class AppColors {


    static const primaryTextColor = black;
    static const secondaryTextColor = gray;
    static const backgroundColor = white;

    static const transparent = const Color(0x00000000);
    static const grayF9 = const Color(0xFFF8F9F9);
    static const grayLight = const Color(0x1F000000);
    static const grayF7 = const Color(0xFFF7F7F7);
    static const gray = const Color(0xFFAFAFAF);
    static const black = const Color(0xFF000000);
    static const white = const Color(0xFFFFFFFF);
    static const green = const Color(0xFF159E49);
    static const greenLight = const Color(0xFFE7F5EC);
    static const red = const Color(0xFFF0583A);
    static const redLight = const Color(0xFFFDEEEB);
    static const blue = const Color(0xFF64AFFF);
    static const blueLight = const Color(0xFFF0F7FF);

}