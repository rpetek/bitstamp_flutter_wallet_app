import 'package:bitstamp_flutter_wallet_app/screens/card/list_item.dart';

class Balance implements ListItem {
    final String balance;

    Balance(this.balance);
}