import 'package:bitstamp_flutter_wallet_app/screens/card/list_item.dart';

class Currency implements ListItem {

    Currency({
        this.symbol,
        this.code,
        this.name
    });

    String symbol;
    String code;
    String name;

    factory Currency.fromJson(Map<String, dynamic> json) {
        return Currency(
            symbol: json["symbol"],
            code: json["code"],
            name: json["name"]
        );
    }

    @override
    String toString() {
        return 'Currency{symbol: $symbol, code: $code, name: $name}';
    }

}