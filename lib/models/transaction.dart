import 'package:bitstamp_flutter_wallet_app/screens/card/list_item.dart';

class Transaction implements ListItem {

    Transaction({
        this.date,
        this.amount,
        this.name,
        this.symbol,
        this.type,
        this.logo
    });

    DateTime date;
    double amount;
    String name;
    String symbol;
    String type;
    String logo;

    factory Transaction.fromJson(Map<String, dynamic> json) {
        return Transaction(
            date: DateTime.parse(json["date"]),
            amount: json["amount"],
            name: json["name"],
            symbol: json["symbol"],
            type: json["type"],
            logo: json["logo"]
        );
    }

    @override
    String toString() {
        return 'Transaction{date: $date, amount: $amount, name: $name, symbol: $symbol, type: $type, logo: $logo}';
    }

}