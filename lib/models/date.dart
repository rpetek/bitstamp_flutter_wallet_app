import 'package:bitstamp_flutter_wallet_app/screens/card/list_item.dart';
import 'package:intl/intl.dart';

class Date implements ListItem {
    final DateTime dateTime;

    Date(this.dateTime);

    String getTimeDisplay() {
        DateTime currentTime = DateTime.now();
        DateTime previousDay = currentTime.subtract(Duration(days: 1));
        if (dateTime.year == currentTime.year &&
            dateTime.month == currentTime.month &&
            dateTime.day == currentTime.day) {
            return "Today";
        } else if (dateTime.year == previousDay.year &&
            dateTime.month == previousDay.month &&
            dateTime.day == previousDay.day) {
            return "Yesterday";
        } else {
            if (dateTime.year == currentTime.year) {
                return DateFormat("d MMM").format(dateTime);
            } else {
                return DateFormat("d MMM yyyy").format(dateTime);
            }
        }
    }
}