class Screens {
    final int value;

    const Screens._internal(this.value);

    toString() => 'Enum.$value';

    static const OVERVIEW = const Screens._internal(0);
    static const SEND = const Screens._internal(1);
    static const SETTINGS = const Screens._internal(2);
}