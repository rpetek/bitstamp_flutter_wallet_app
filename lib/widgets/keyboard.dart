import 'package:bitstamp_flutter_wallet_app/colors.dart';
import 'package:bitstamp_flutter_wallet_app/resources/assets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Keyboard extends StatelessWidget {

    final bool showDecimal;
    final Function(Object value) onTap;
    final Function(Object value) onLongTap;

    const Keyboard({Key key,
        this.showDecimal = true,
        this.onTap,
        this.onLongTap
    }) : super(key: key);

    @override
    Widget build(BuildContext context) {
        return Column(
            children: <Widget>[
                Expanded(
                    child: Row(
                        children: <Widget>[
                            KeyboardButton(value: "1", onTap: onTap),
                            KeyboardButton(value: "2", onTap: onTap),
                            KeyboardButton(value: "3", onTap: onTap)
                        ],
                    ),
                ),
                Expanded(
                    child: Row(
                        children: <Widget>[
                            KeyboardButton(value: "4", onTap: onTap),
                            KeyboardButton(value: "5", onTap: onTap),
                            KeyboardButton(value: "6", onTap: onTap)
                        ],
                    ),
                ),
                Expanded(
                    child: Row(
                        children: <Widget>[
                            KeyboardButton(value: "7", onTap: onTap),
                            KeyboardButton(value: "8", onTap: onTap),
                            KeyboardButton(value: "9", onTap: onTap)
                        ],
                    ),
                ),
                Expanded(
                    child: Row(
                        children: <Widget>[
                            Visibility(
                                child: KeyboardButton(value: ".", onTap: onTap),
                                replacement: Expanded(child: Container()),
                                visible: showDecimal,
                            ),
                            KeyboardButton(value: "0", onTap: onTap),
                            KeyboardButtonAction(
                                asset: Assets.ic_backspace,
                                type: KeyboardActionType.BACKSPACE,
                                onTap: onTap,
                                onLongTap: onLongTap,)
                        ],
                    )
                ),
            ],
        );
    }

}

class KeyboardButton extends StatelessWidget {
    final String value;
    final Color textColor;
    final double fontSize;

    final Function(String value) onTap;

    const KeyboardButton({Key key,
        this.value,
        this.onTap,
        this.textColor = AppColors.primaryTextColor,
        this.fontSize = 20})
        : super(key: key);

    @override
    Widget build(BuildContext context) {
        return Expanded(
            child: Material(
                color: AppColors.transparent,
                child: InkWell(
                    highlightColor: AppColors.transparent,
                    splashColor: AppColors.grayLight,
                    onTap: () {
                        if (onTap != null) {
                            onTap(value);
                        }
                    },
                    child: Center(
                        child: Text(value, style: TextStyle(
                            color: textColor, fontSize: fontSize),
                        ),
                    )
                ),
            ),
        );
    }
}

enum KeyboardActionType {
    BACKSPACE
}

class KeyboardButtonAction extends StatelessWidget {
    final String asset;
    final Color iconColor;
    final KeyboardActionType type;

    final Function(KeyboardActionType type) onTap;
    final Function(KeyboardActionType type) onLongTap;

    const KeyboardButtonAction({Key key,
        this.asset,
        this.onTap,
        this.onLongTap,
        this.type,
        this.iconColor = AppColors.primaryTextColor})
        : super(key: key);

    @override
    Widget build(BuildContext context) {
        return Expanded(
            child: SizedBox(
                child: Material(
                    color: AppColors.transparent,
                    child: InkWell(
                        splashColor: AppColors.grayLight,
                        onTap: () {
                            if (onTap != null) {
                                onTap(type);
                            }
                        },
                        onLongPress: () {
                            if (onLongTap != null) {
                                onLongTap(type);
                            }
                        },
                        child: Center(
                            child: SvgPicture.asset(asset, color: iconColor),
                        )
                    ),
                ),
            )
        );
    }
}