import 'package:bitstamp_flutter_wallet_app/colors.dart';
import 'package:bitstamp_flutter_wallet_app/styles.dart';
import 'package:flutter/material.dart';

class PinWidget extends StatelessWidget {

    final String pinCode;
    final int maxPinLength;

    const PinWidget({Key key,
        @required this.pinCode,
        @required this.maxPinLength})
        :super(key: key);

    String _getPinNum(String pinCode, int idx) {
        if (idx < pinCode.length) {
            return pinCode[idx];
        } else {
            return "";
        }
    }

    bool _isActivated(String pinCode, int idx) {
        return _getPinNum(pinCode, idx).length > 0;
    }

    @override
    Widget build(BuildContext context) {
        return Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
                PinItemWidget(value: _getPinNum(pinCode, 0),
                    activated: _isActivated(pinCode, 0)),
                PinItemWidget(value: _getPinNum(pinCode, 1),
                    activated: _isActivated(pinCode, 1)),
                PinItemWidget(value: _getPinNum(pinCode, 2),
                    activated: _isActivated(pinCode, 2)),
                PinItemWidget(value: _getPinNum(pinCode, 3),
                    activated: _isActivated(pinCode, 3),
                    paddingRight: 0)
            ]
        );
    }

}

class PinItemWidget extends StatelessWidget {

    final String value;
    final bool activated;
    final double paddingRight;

    const PinItemWidget({Key key,
        @required this.value,
        @required this.activated,
        this.paddingRight = 15
    }) :super(key: key);

    @override
    Widget build(BuildContext context) {
        return
            Padding(
                padding: EdgeInsets.only(right: paddingRight),
                child: Container(
                    width: 40,
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                            Center(
                                child: Text(
                                    value, style: AppTextStyles.textPin),
                            ),
                            SizedBox(height: 10),
                            AnimatedOpacity(
                                duration: Duration(milliseconds: 250),
                                opacity: activated ? 1.0 : 0.2,
                                child: Container(
                                    height: 1,
                                    color: AppColors.black,
                                )
                            )
                        ]
                    )
                ),
            );
    }

}