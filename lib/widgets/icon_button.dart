import 'package:bitstamp_flutter_wallet_app/colors.dart';
import 'package:bitstamp_flutter_wallet_app/widgets/splash_effect.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class AppIconButton extends StatelessWidget {
    final String icon;
    final Color color;
    final Color colorBckg;
    final double width;
    final double height;
    final double radius;
    final Function onTap;

    const AppIconButton(
        {Key key, this.icon, this.color, this.colorBckg, this.width, this.height, this.radius, this.onTap})
        : super(key: key);

    @override
    Widget build(BuildContext context) {
        return ClipRRect(
            borderRadius: BorderRadius.circular(radius ?? 25.0),
            child: Container(
                width: width ?? 35,
                height: height ?? 35,
                child: SplashEffect(
                    color: this.colorBckg,
                    onTap: () {
                        if (onTap != null) {
                            onTap();
                        }
                    },
                    child: Padding(
                        padding: EdgeInsets.all(8),
                        child: SvgPicture.asset(
                            icon,
                            color: this.color ?? AppColors.primaryTextColor
                        ),
                    )
                ),
            ),
        );
    }
}