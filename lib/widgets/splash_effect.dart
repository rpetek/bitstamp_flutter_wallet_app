import 'package:bitstamp_flutter_wallet_app/colors.dart';
import 'package:flutter/material.dart';

class SplashEffect extends StatelessWidget {
    final Widget child;
    final Color color;
    final Function onTap;

    const SplashEffect({Key key, this.child, this.color, this.onTap})
        : super(key: key);

    @override
    Widget build(BuildContext context) {
        return Material(
            child: Ink(
                color: this.color ?? AppColors.white,
                child:
                InkWell(
                    onTap: () {
                        if (onTap != null) {
                            onTap();
                        }
                    },
                    child: child, // other widget
                ),
            ),
        );
    }
}