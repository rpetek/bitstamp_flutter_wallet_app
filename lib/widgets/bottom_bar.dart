import 'package:bitstamp_flutter_wallet_app/colors.dart';
import 'package:bitstamp_flutter_wallet_app/enums/screens.dart';
import 'package:bitstamp_flutter_wallet_app/resources/assets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class BottomBarItem extends StatelessWidget {
    final String title;
    final Widget icon;
    final Function onTap;
    final Screens screen;
    final Color color;

    const BottomBarItem({
        Key key,
        this.title,
        this.icon,
        this.onTap,
        @required this.screen,
        @required this.color,
    }) : super(key: key);

    @override
    Widget build(BuildContext context) {
        return Expanded(
            child: InkWell(
                onTap: () {
                    if (this.onTap != null) {
                        this.onTap(screen);
                    }
                },
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                        this.icon,
                        SizedBox(
                            height: 5.0,
                        ),
                        Visibility(
                            visible: title != null,
                            child: Text(title ?? "",
                                style: TextStyle(color: color, fontSize: 12.0)),
                        )

                    ],
                ),
            ),
        );
    }
}

class BottomBar extends StatefulWidget {
    final Function onTabChange;
    @required final Color color;
    @required final Color colorActive;
    final double height;
    final BoxDecoration background;

    const BottomBar({
        Key key,
        this.onTabChange,
        this.color,
        this.colorActive,
        this.height = 60.0,
        this.background,
    }) : super(key: key);

    @override
    _BottomBarState createState() => _BottomBarState();

}

class _BottomBarState extends State<BottomBar> {

    Screens currentScreen = Screens.OVERVIEW;

    handleTabChange(id) {
        widget.onTabChange(id);
        setState(() {
            if (id != Screens.SEND) {
                currentScreen = id;
            }
        });
    }

    getItemColor(Screens screen) {
        return currentScreen == screen ? widget.colorActive : widget.color;
    }

    @override
    Widget build(BuildContext context) {
        var bg = widget.background;

        const String iconCard = Assets.ic_card;
        const String iconSend = Assets.ic_send;
        const String iconSettings = Assets.ic_settings;

        return Container(
            decoration: bg,
            child: Padding(
                padding: EdgeInsets.only(bottom: MediaQuery
                    .of(context)
                    .padding
                    .bottom),
                child: Container(
                    height: widget.height,
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                            BottomBarItem(
                                title: "Card",
                                icon: SvgPicture.asset(
                                    iconCard,
                                    color: getItemColor(Screens.OVERVIEW)
                                ),
                                screen: Screens.OVERVIEW,
                                onTap: handleTabChange,
                                color: getItemColor(Screens.OVERVIEW),
                            ),
                            BottomBarItem(
                                icon: ClipRRect(
                                    borderRadius: BorderRadius.circular(25.0),
                                    child:
                                    Container(
                                        width: 50,
                                        height: 50,
                                        color: AppColors.green,
                                        child: Padding(
                                            padding: EdgeInsets.all(16),
                                            child: SvgPicture.asset(
                                                iconSend,
                                                color: AppColors.white
                                            ),
                                        ),
                                    )
                                ),
                                screen: Screens.SEND,
                                onTap: handleTabChange,
                                color: getItemColor(Screens.SEND),
                            ),
                            BottomBarItem(
                                title: "Settings",
                                icon: SvgPicture.asset(
                                    iconSettings,
                                    color: getItemColor(Screens.SETTINGS)
                                ),
                                screen: Screens.SETTINGS,
                                onTap: handleTabChange,
                                color: getItemColor(Screens.SETTINGS),
                            ),
                        ],
                    ),
                ),
            ),
        );
    }
}
