import 'package:flutter/material.dart';

class ButtonElevation extends StatelessWidget {
    final Widget child;
    final double height;
    final Color color;

    ButtonElevation({
        @required this.child,
        @required this.height,
        @required this.color}) : assert(child != null);

    @override
    Widget build(BuildContext context) {
        return Container(
            width: double.infinity,
            height: this.height,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(
                    Radius.circular(this.height / 2)),
                boxShadow: <BoxShadow>[
                    BoxShadow(
                        color: this.color.withOpacity(0.2),
                        blurRadius: height / 10,
                        offset: Offset(0, height / 10),
                    ),
                ],
            ),
            child: this.child,
        );
    }
}