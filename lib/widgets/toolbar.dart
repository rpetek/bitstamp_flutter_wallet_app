import 'package:bitstamp_flutter_wallet_app/colors.dart';
import 'package:bitstamp_flutter_wallet_app/styles.dart';
import 'package:bitstamp_flutter_wallet_app/widgets/icon_button.dart';
import 'package:flutter/material.dart';

class Toolbar extends StatelessWidget implements PreferredSizeWidget {

    final PreferredSizeWidget bottom;
    final String iconNav;
    final String iconAction;
    final String title;

    final Color navIconColor;
    final Color navIconBckgColor;
    final Color actionIconColor;
    final Color actionIconBckgColor;

    final Function onNavTap;
    final Function onActionTap;

    const Toolbar({Key key,
        this.title,
        this.iconNav,
        this.iconAction,
        this.onNavTap,
        this.onActionTap,
        this.navIconColor,
        this.navIconBckgColor,
        this.actionIconColor,
        this.actionIconBckgColor,
        this.bottom}) : super(key: key);

    @override
    Widget build(BuildContext context) {
        return SafeArea(
            child: Container(
                height: 56,
                color: AppColors.white,
                child: Padding(
                    padding: EdgeInsets.only(left: AppPadding.paddingSmall,
                        right: AppPadding.paddingSmall),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                            Visibility(
                                visible: this.iconNav != null,
                                child: AppIconButton(
                                    icon: iconNav,
                                    color: navIconColor,
                                    colorBckg: navIconBckgColor,
                                    onTap: () {
                                        if (this.onNavTap == null) {
                                            Navigator.pop(context);
                                        } else {
                                            this.onNavTap();
                                        }
                                    },
                                ),
                                replacement: Container(width: 40)
                            ),
                            Expanded(
                                child: Center(
                                    child: Text(
                                        this.title, style: TextStyle(
                                        color: AppColors.primaryTextColor,
                                        fontSize: 18)
                                    )
                                )
                            ),
                            Visibility(
                                visible: this.iconAction != null,
                                child: AppIconButton(
                                    icon: iconAction,
                                    color: actionIconColor,
                                    colorBckg: actionIconBckgColor,
                                    onTap: () {
                                        if (this.onActionTap == null) {
                                            Navigator.pop(context);
                                        } else {
                                            this.onActionTap();
                                        }
                                    },
                                ),
                                replacement: Container(width: 40)
                            )

                        ],
                    ),
                )
            )
        );
    }

    @override
    Size get preferredSize =>
        Size.fromHeight(
            kToolbarHeight + (bottom?.preferredSize?.height ?? 0.0));
}