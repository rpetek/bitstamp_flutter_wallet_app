import 'package:bitstamp_flutter_wallet_app/colors.dart';
import 'package:bitstamp_flutter_wallet_app/main.dart';
import 'package:bitstamp_flutter_wallet_app/models/currency.dart';
import 'package:bitstamp_flutter_wallet_app/resources/assets.dart';
import 'package:bitstamp_flutter_wallet_app/resources/screens.dart';
import 'package:bitstamp_flutter_wallet_app/resources/strings.dart';
import 'package:bitstamp_flutter_wallet_app/styles.dart';
import 'package:bitstamp_flutter_wallet_app/widgets/button_elevation.dart';
import 'package:bitstamp_flutter_wallet_app/widgets/center_horizontal.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class ResponseScreen extends StatefulWidget {

    final ActionType actionType;
    final Contact contact;
    final double amount;
    final Currency currency;

    const ResponseScreen({Key key,
        @required this.actionType,
        @required this.contact,
        @required this.amount,
        @required this.currency})
        : super(key: key);

    @override
    _ResponseScreenState createState() => _ResponseScreenState();
}

class _ResponseScreenState extends State<ResponseScreen> {

    String pinCode = "";
    int pinLength = 4;

    @override
    void initState() {
        super.initState();
    }

    Widget _getAvatar() {
        return Stack(
            children: <Widget>[
                SizedBox(
                    width: 38,
                    height: 38,
                    child: (widget.contact.avatar != null &&
                        widget.contact.avatar.length > 0)
                        ? CircleAvatar(
                        backgroundImage: MemoryImage(widget.contact.avatar))
                        : CircleAvatar(child: Text(widget.contact.initials())),
                )
            ],
        );
    }

    Widget _getName() {
        return Padding(
            padding: EdgeInsets.only(left: AppPadding.paddingSmall),
            child: Text(widget.contact.displayName ?? "")
        );
    }

    Widget _getAmount() {
        return
            ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(6)),
                child: Container(
                    color: AppColors.greenLight,
                    child: Padding(
                        padding: EdgeInsets.only(
                            top: AppPadding.padding4,
                            bottom: AppPadding.padding4,
                            left: AppPadding.padding6,
                            right: AppPadding.padding6),
                        child: Text(
                            widget.currency.symbol + widget.amount.toString(),
                            style: TextStyle(color: AppColors.green),)
                    ),
                ),
            );
    }

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            backgroundColor: AppColors.white,
            body: SafeArea(
                child: Padding(padding: EdgeInsets.only(
                    left: AppPadding.padding,
                    right: AppPadding.padding,
                    top: AppPadding.paddingTriple,
                    bottom: AppPadding.padding),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                            CenterHorizontal(
                                Text(
                                    Strings.response_success_title,
                                    style: AppTextStyles.textSuccess
                                )
                            ),
                            Padding(
                                padding: EdgeInsets.only(
                                    top: AppPadding.paddingBig,
                                    bottom: AppPadding.paddingDouble
                                ),
                                child: SvgPicture.asset(Assets.ic_success)
                            ),
                            CenterHorizontal(
                                Text(Strings.response_success_subtitle,
                                    style: AppTextStyles.textBig)
                            ),
                            Padding(
                                padding: EdgeInsets.only(
                                    top: AppPadding.paddingBig
                                ),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment
                                        .spaceBetween,
                                    children: <Widget>[
                                        Wrap(
                                            crossAxisAlignment: WrapCrossAlignment
                                                .center,
                                            children: <Widget>[
                                                _getAvatar(),
                                                _getName()
                                            ],
                                        ),
                                        _getAmount()
                                    ],
                                ),
                            ),
                            Expanded(
                                child: Align(
                                    alignment: FractionalOffset.bottomCenter,
                                    child: ButtonElevation(
                                        height: 46,
                                        color: AppColors.green,
                                        child: FlatButton(
                                            shape: StadiumBorder(),
                                            color: AppColors.green,
                                            onPressed: () {
                                                Navigator.popUntil(
                                                    context, ModalRoute
                                                    .withName(
                                                    AppScreens.screen_home));
                                            },
                                            child: Text(
                                                Strings.response_success_action,
                                                style: AppTextStyles
                                                    .textButton),
                                        )
                                    ),
                                ),
                            )


                        ],
                    ),
                )

            )
        );
    }
}
