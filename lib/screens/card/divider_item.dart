import 'package:bitstamp_flutter_wallet_app/colors.dart';
import 'package:bitstamp_flutter_wallet_app/models/date.dart';
import 'package:bitstamp_flutter_wallet_app/styles.dart';
import 'package:flutter/material.dart';

class DividerItem extends StatelessWidget {
    final Date date;

    const DividerItem({ @required this.date }) : super();

    @override
    Widget build(BuildContext context) {
        return Container(
            height: 30,
            color: AppColors.grayF7,
            child: Center(child: Text(date.getTimeDisplay(),
                textAlign: TextAlign.center,
                style: AppTextStyles
                    .textItemSubtitle),
            )
        );
    }
}