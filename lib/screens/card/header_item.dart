import 'package:bitstamp_flutter_wallet_app/colors.dart';
import 'package:bitstamp_flutter_wallet_app/styles.dart';
import 'package:bitstamp_flutter_wallet_app/resources/assets.dart';
import 'package:bitstamp_flutter_wallet_app/widgets/button_elevation.dart';
import 'package:bitstamp_flutter_wallet_app/widgets/center_horizontal.dart';
import 'package:bitstamp_flutter_wallet_app/widgets/splash_effect.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class HeaderItem extends StatelessWidget {
    final String balance;
    final Function onCardSettingsTap;
    final String iconCard = Assets.ic_card_sm;

    const HeaderItem({ @required this.balance, this.onCardSettingsTap })
        : super();

    @override
    Widget build(BuildContext context) {
        return Container(
            child: Padding(
                padding: EdgeInsets.only(
                    left: AppPadding.padding,
                    right: AppPadding.padding,
                    top: AppPadding.padding,
                    bottom: AppPadding.paddingBig
                ),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                        Padding(
                            padding: EdgeInsets.only(
                                left: AppPadding.padding,
                                right: AppPadding.padding,
                                bottom: AppPadding.paddingBig
                            ),
                            child: Stack(
                                overflow: Overflow.visible,
                                fit: StackFit.loose,
                                children: <Widget>[
                                    Container(
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(40)),
                                            boxShadow: <BoxShadow>[
                                                BoxShadow(
                                                    color: AppColors.black
                                                        .withOpacity(
                                                        0.15),
                                                    blurRadius: 40 / 5,
                                                    offset: Offset(0, 40 / 5),
                                                ),
                                            ],
                                        ),
                                        child: SvgPicture.asset(iconCard),
                                    ),
                                    Positioned(
                                        bottom: 11,
                                        left: 80,
                                        child: Text("442", style: TextStyle(
                                            color: AppColors.gray)
                                        ),
                                    ),
                                    Positioned(
                                        top: -15,
                                        right: -20,
                                        child: SettingsButton(
                                            onTap: () {
                                                onCardSettingsTap();
                                            },
                                        ),
                                    ),
                                ]
                            ),
                        ),
                        CenterHorizontal(
                            Text(
                                balance,
                                style: AppTextStyles.textLarge
                            )
                        ),
                        CenterHorizontal(
                            Text(
                                "Total balance",
                                style: AppTextStyles.textSmall
                            )
                        ),
                        SizedBox(height: AppPadding.paddingBig),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                                Expanded(
                                    child: Padding(
                                        padding: EdgeInsets.only(
                                            right: AppPadding.paddingMini),
                                        child: ButtonElevation(
                                            height: 40,
                                            color: AppColors.green,
                                            child: FlatButton(
                                                shape: StadiumBorder(),
                                                color: AppColors.green,
                                                onPressed: () {},
                                                child: Text("Top up",
                                                    style: AppTextStyles
                                                        .textButton),
                                            ),
                                        )
                                    )
                                ),
                                Expanded(
                                    child: Padding(
                                        padding: EdgeInsets.only(
                                            left: AppPadding.paddingMini),
                                        child: ButtonElevation(
                                            height: 40,
                                            color: AppColors.red,
                                            child: FlatButton(
                                                shape: StadiumBorder(),
                                                color: AppColors.red,
                                                onPressed: () {},
                                                child: Text("Cash out",
                                                    style: AppTextStyles
                                                        .textButton),
                                            ),
                                        )
                                    )
                                )
                            ],
                        )

                    ],
                )
            )
        );
    }
}

class SettingsButton extends StatelessWidget {

    final Function onTap;
    final String iconSettings = Assets.ic_settings;

    const SettingsButton({Key key, this.onTap}) : super(key: key);

    @override
    Widget build(BuildContext context) {
        return Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(
                    Radius.circular(40)),
                boxShadow: <BoxShadow>[
                    BoxShadow(
                        color: AppColors.black
                            .withOpacity(
                            0.10),
                        blurRadius: 3,
                        offset: Offset(3, 3),
                    ),
                ],
            ),
            child: ClipRRect(
                borderRadius: BorderRadius.circular(25.0),
                child: Container(
                    width: 40,
                    height: 40,
                    color: AppColors.white,
                    child: SplashEffect(
                        onTap: () {
                            if (onTap != null) {
                                onTap();
                            }
                        },
                        child: Padding(
                            padding: EdgeInsets.all(8),
                            child: SvgPicture.asset(
                                iconSettings,
                                color: AppColors.gray
                            ),
                        )
                    ),
                ),
            ),
        );
    }
}