import 'package:bitstamp_flutter_wallet_app/colors.dart';
import 'package:bitstamp_flutter_wallet_app/models/transaction.dart';
import 'package:bitstamp_flutter_wallet_app/styles.dart';
import 'package:bitstamp_flutter_wallet_app/resources/assets.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';

class TransactionItem extends StatelessWidget {
    final Transaction transaction;

    const TransactionItem({ @required this.transaction }) : super();

    String _formatAmount(Transaction transaction) {
        if (transaction.type == "transaction") {
            return "- \$${transaction.amount}";
        } else {
            return "\$${transaction.amount}";
        }
    }

    Widget _getImage() {
        if (transaction.type == "transaction") {
            return Image.network(transaction.logo);
        } else {
            String iconDeposit = Assets.ic_deposit;
            return Padding(padding: EdgeInsets.all(4),
                child: SvgPicture.asset(
                    iconDeposit,
                    color: AppColors.green
                ));
        }
    }

    @override
    Widget build(BuildContext context) {
        return Container(
            height: 80,
            child: Padding(
                padding: EdgeInsets.only(
                    left: AppPadding.padding, right: AppPadding.padding),
                child: Row(
                    children: <Widget>[
                        ClipRRect(
                            borderRadius: BorderRadius.circular(30.0),
                            child:
                            Container(
                                width: 30,
                                height: 30,
                                color: AppColors.grayF7,
                                child: Padding(
                                    padding: EdgeInsets.all(4),
                                    child: _getImage(),
                                ),
                            )
                        ),
                        Expanded(child: Padding(
                            padding: EdgeInsets.only(
                                left: AppPadding.paddingMini),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                    Row(
                                        mainAxisAlignment: MainAxisAlignment
                                            .spaceBetween,
                                        children: <Widget>[
                                            Text(transaction.name,
                                                style: AppTextStyles
                                                    .textItemTitle),
                                            Text(
                                                _formatAmount(transaction),
                                                style: AppTextStyles
                                                    .textItemTitle)
                                        ],
                                    ),
                                    Text(formatDate(
                                        transaction.date, [HH, ':', mm]),
                                        style: AppTextStyles.textItemSubtitle,)
                                ],
                            ),
                        ),
                        ),

                    ],
                )
            )
        );
    }
}