import 'package:bitstamp_flutter_wallet_app/colors.dart';
import 'package:bitstamp_flutter_wallet_app/styles.dart';
import 'package:bitstamp_flutter_wallet_app/resources/assets.dart';
import 'package:bitstamp_flutter_wallet_app/widgets/splash_effect.dart';
import 'package:bitstamp_flutter_wallet_app/widgets/toolbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CardSettingsScreen extends StatelessWidget {
    final Widget child;
    final Function onTap;
    final iconLock = Assets.ic_lock;
    final iconReset = Assets.ic_reset;

    const CardSettingsScreen({Key key, this.child, this.onTap})
        : super(key: key);

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            backgroundColor: AppColors.white,
            appBar: Toolbar(
                title: "Card settings", iconAction: Assets.ic_close),
            body: SingleChildScrollView(
                child: Container(
                    child: Padding(
                        padding: EdgeInsets.only(top: AppPadding.padding),
                        child: Center(
                            child: Column(
                                children: <Widget>[
                                    CardHolder(),
                                    Padding(
                                        padding: EdgeInsets.all(
                                            AppPadding.paddingBig),
                                        child: Row(
                                            mainAxisAlignment: MainAxisAlignment
                                                .center,
                                            children: <Widget>[
                                                Column(
                                                    children: <Widget>[
                                                        Container(
                                                            width: 30,
                                                            height: 30,
                                                            child: SvgPicture
                                                                .asset(
                                                                iconLock,
                                                                color: AppColors
                                                                    .grayLight),
                                                        ),
                                                        Padding(
                                                            padding: EdgeInsets
                                                                .only(
                                                                top: AppPadding
                                                                    .paddingSmall),
                                                            child: Text(
                                                                "Lock card",
                                                                style: TextStyle(
                                                                    color: AppColors
                                                                        .grayLight,
                                                                    fontSize: 12),
                                                            )
                                                        )
                                                    ],
                                                ),
                                                Padding(
                                                    padding: EdgeInsets.only(
                                                        left: 40,
                                                        right: 40),
                                                    child: Container(
                                                        color: AppColors
                                                            .grayLight,
                                                        width: 1,
                                                        height: 40,
                                                    )
                                                ),
                                                Column(
                                                    children: <Widget>[
                                                        Container(
                                                            width: 30,
                                                            height: 30,
                                                            child: SvgPicture
                                                                .asset(
                                                                iconReset,
                                                                color: AppColors
                                                                    .grayLight),
                                                        ),
                                                        Padding(
                                                            padding: EdgeInsets
                                                                .only(
                                                                top: AppPadding
                                                                    .paddingSmall),
                                                            child: Text(
                                                                "Reset PIN",
                                                                style: TextStyle(
                                                                    color: AppColors
                                                                        .grayLight,
                                                                    fontSize: 12),
                                                            )
                                                        )
                                                    ],
                                                )
                                            ],
                                        )
                                    ),
                                    SettingsSwitchItem(
                                        title: "Online payments"),
                                    SettingsSwitchItem(
                                        title: "ATM withdrawals"),
                                    SettingsItem(title: "Limits",
                                        subtitle: "Set withdrawal and payment limits"),
                                    SettingsItem(title: "Smart convert",
                                        subtitle: "BTC, ETH, LTC, BCH, XRP")
                                ],
                            )),
                    ),
                )
            )
        );
    }
}

class CardHolder extends StatelessWidget {

    final String iconCard = Assets.ic_card_lg;

    @override
    Widget build(BuildContext context) {
        return Stack(
            children: <Widget>[
                Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                            Radius.circular(40)),
                        boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: AppColors.black
                                    .withOpacity(
                                    0.15),
                                blurRadius: 40 / 5,
                                offset: Offset(
                                    0, 40 / 5),
                            ),
                        ],
                    ),
                    child: SvgPicture.asset(iconCard),
                ),
                Positioned(
                    bottom: 60,
                    left: 20,
                    child: Text(
                        "1234 5678 9101 442",
                        style: TextStyle(
                            color: AppColors.gray,
                            fontSize: 16)
                    ),
                ),
                Positioned(
                    bottom: 20,
                    left: 20,
                    child: Text(
                        "Norman Armstrong",
                        style: TextStyle(
                            color: AppColors.gray,
                            fontSize: 12)
                    ),
                ),
                Positioned(
                    bottom: 20,
                    right: 20,
                    child: Text(
                        "11/21",
                        style: TextStyle(
                            color: AppColors.gray,
                            fontSize: 12)
                    ),
                )
            ],
        );
    }
}

class SettingsSwitchItem extends StatefulWidget {

    final String title;

    const SettingsSwitchItem({Key key, @required this.title}) : super(key: key);

    @override
    _SettingsSwitchItemState createState() => _SettingsSwitchItemState();
}

class _SettingsSwitchItemState extends State<SettingsSwitchItem> {

    bool selected = false;

    handleSwitch() {
        setState(() {
            selected = !selected;
        });
    }

    @override
    Widget build(BuildContext context) {
        return SplashEffect(
            onTap: () {
                handleSwitch();
            },
            child: Container(
                height: 70,
                child: Padding(
                    padding: EdgeInsets.only(
                        left: AppPadding.paddingBig,
                        right: AppPadding.paddingBig
                    ),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                            Text(widget.title,
                                style: AppTextStyles.textItemTitle),
                            Switch(
                                value: selected,
                                onChanged: (bool value) {
                                    handleSwitch();
                                },
                                activeTrackColor: AppColors.green,
                                activeColor: AppColors.white,
                            )
                        ],
                    )
                ),
            )
        );
    }
}

class SettingsItem extends StatelessWidget {

    final String title;
    final String subtitle;

    const SettingsItem({Key key, @required this.title, @required this.subtitle})
        : super(key: key);

    @override
    Widget build(BuildContext context) {
        return SplashEffect(
            child: Container(
                height: 70,
                child: Padding(
                    padding: EdgeInsets.only(
                        left: AppPadding.paddingBig,
                        right: AppPadding.paddingBig
                    ),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                            Text(title, style: AppTextStyles.textItemTitle),
                            Text(subtitle,
                                style: AppTextStyles.textItemSubtitle),
                        ],
                    )
                )
            ),
        );
    }
}