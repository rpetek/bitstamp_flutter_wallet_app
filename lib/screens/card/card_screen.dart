import 'dart:convert';

import 'package:bitstamp_flutter_wallet_app/models/balance.dart';
import 'package:bitstamp_flutter_wallet_app/models/date.dart';
import 'package:bitstamp_flutter_wallet_app/models/transaction.dart';
import 'package:bitstamp_flutter_wallet_app/resources/screens.dart';
import 'package:bitstamp_flutter_wallet_app/screens/card/card_settings_screen.dart';
import 'package:bitstamp_flutter_wallet_app/screens/card/divider_item.dart';
import 'package:bitstamp_flutter_wallet_app/screens/card/header_item.dart';
import 'package:bitstamp_flutter_wallet_app/screens/card/transaction_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:jiffy/jiffy.dart';

import 'list_item.dart';

class CardScreen extends StatefulWidget {
    const CardScreen({Key key}) : super(key: key);

    @override
    _CardScreenState createState() => _CardScreenState();
}

class _CardScreenState extends State<CardScreen> {
    final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey = new GlobalKey<
        RefreshIndicatorState>();

    final List<ListItem> _data = List();

    @override
    initState() {
        super.initState();
        _refresh();
    }

    @override
    void dispose() {
        super.dispose();
    }

    Future<Null> _refresh() async {
        // holding pull to refresh loader widget for 1 sec.
        await Future.delayed(const Duration(seconds: 0));
        _data.clear();

        var jsonData = await rootBundle.loadString(
            'assets/data/transactions.json');
        /*var jsonData = await DefaultAssetBundle.of(context).loadString(
            'assets/data/transactions.json');*/
        // decode the JSON
        var jsonDecoded = json.decode(jsonData.toString());
        List<Transaction> transactions = List.of(jsonDecoded)
            .map((i) => Transaction.fromJson(i))
            .toList();
        setState(() => _data.addAll(groupItems(transactions)));
        return null;
    }

    List<ListItem> groupItems(List<Transaction> transactions) {
        // sort descending by date
        transactions.sort((a, b) => b.date.compareTo(a.date));

        Map<int, List<Transaction>> transactionMap = Map();
        transactions.forEach((item) {
            // key must consist of day of the year and the year itself
            var key = Jiffy(item.date).dayOfYear + Jiffy(item.date).year;
            if (transactionMap.containsKey(key)) {
                transactionMap[key].add(item);
            } else {
                transactionMap[key] = List<Transaction>();
                transactionMap[key].add(item);
            }
        });

        List<ListItem> groupedItems = List();
        groupedItems.add(Balance("\$ 4.869,00"));
        transactionMap.forEach((k, v) {
            groupedItems.add(Date(transactionMap[k][0].date));
            transactionMap[k].forEach((item) {
                groupedItems.add(item);
            });
        });

        return groupedItems;
    }

    @override
    Widget build(BuildContext context) {
        return RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: _refresh,
            // use future builder and DefaultAssetBundle to load the local JSON file
            child: ListView.builder(
                itemCount: _data == null ? 0 : _data.length,
                itemBuilder: (BuildContext context, int index) {
                    final item = _data[index];
                    if (item is Balance) {
                        return HeaderItem(
                            onCardSettingsTap: () {
                                print("card settings");
                                Navigator.of(context).push(
                                    new MaterialPageRoute<Null>(
                                        settings: RouteSettings(
                                            name: AppScreens
                                                .screen_card_settings),
                                        builder: (BuildContext context) {
                                            return CardSettingsScreen();
                                        }
                                    ));
                            },
                            balance: item.balance);
                    } else if (item is Date) {
                        return DividerItem(
                            date: item);
                    } else if (item is Transaction) {
                        return TransactionItem(
                            transaction: item);
                    } else {
                        return Container();
                    }
                }
            )
        );
    }

/*@override
    Widget build(BuildContext context) {
        return RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: _refresh,
            // use future builder and DefaultAssetBundle to load the local JSON file
            child: FutureBuilder(
                future: DefaultAssetBundle
                    .of(context)
                    .loadString('assets/data/transactions.json'),
                builder: (context, snapshot) {
                    // decode the JSON
                    var jsonData = json.decode(snapshot.data.toString());
                    List<Transaction> transactions = List.of(jsonData)
                        .map((i) => Transaction.fromJson(i))
                        .toList();
                    var data = groupItems(transactions);

                    return ListView.builder(
                        itemCount: data == null ? 0 : data.length,
                        itemBuilder: (BuildContext context, int index) {
                            final item = data[index];
                            if (item is Balance) {
                                return HeaderItem(
                                    balance: item.balance);
                            } else if (item is Date) {
                                return DividerItem(
                                    date: item);
                            } else if (item is Transaction) {
                                return TransactionItem(
                                    transaction: item);
                            } else {
                                return Container();
                            }
                        }
                    );
                })
        );
    }*/
}