import 'package:bitstamp_flutter_wallet_app/colors.dart';
import 'package:bitstamp_flutter_wallet_app/resources/assets.dart';
import 'package:bitstamp_flutter_wallet_app/styles.dart';
import 'package:bitstamp_flutter_wallet_app/widgets/icon_button.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class ContactList extends StatelessWidget {

    final GlobalKey listKey;
    final List<ContactItem> contacts;
    final Function(Contact) onItemClick;
    final Function(Contact) onAddContact;
    final Function(Contact) onRemoveContact;

    const ContactList({Key key,
        @required this.listKey,
        @required this.contacts,
        @required this.onItemClick,
        @required this.onAddContact,
        @required this.onRemoveContact})
        :super(key: key);

    @override
    Widget build(BuildContext context) {
        return contacts != null
            ? ListView.builder(
            itemCount: contacts?.length ?? 0,
            itemBuilder: (BuildContext context, int index) {
                ContactItem c = contacts?.elementAt(index);

                if (c is ContactHeaderItem) {
                    return ContactHeaderItemView(
                        title: c.title, numOfItems: c.numOfItems);
                } else if (c is ContactBitstampItem) {
                    return ContactItemView(
                        contact: c.contact,
                        isSaved: true,
                        onItemClick: onItemClick,
                        onRemoveContact: onRemoveContact);
                } else if (c is ContactOtherItem) {
                    return ContactItemView(
                        contact: c.contact,
                        isSaved: false,
                        onAddContact: onAddContact);
                } else {
                    return null;
                }
            },
        ) : Container();
    }

}

abstract class ContactItem {
}

class ContactHeaderItem implements ContactItem {
    final String title;
    final int numOfItems;

    const ContactHeaderItem({@required this.title, @required this.numOfItems});
}

class ContactBitstampItem implements ContactItem {
    final Contact contact;

    const ContactBitstampItem({@required this.contact});
}

class ContactOtherItem implements ContactItem {
    final Contact contact;

    const ContactOtherItem({@required this.contact});
}


class ContactHeaderItemView extends StatelessWidget {

    final String title;
    final int numOfItems;

    const ContactHeaderItemView({Key key, this.title, this.numOfItems})
        :super(key: key);

    getTitle() {
        return title + " ($numOfItems)";
    }

    getNumOfItems() {
        return numOfItems;
    }

    @override
    Widget build(BuildContext context) {
        return Padding(
            padding: EdgeInsets.only(
                top: AppPadding.paddingMini, bottom: AppPadding.paddingMini),
            child: Container(
                color: AppColors.grayF9,
                child: Padding(
                    padding: EdgeInsets.only(top: AppPadding.padding6,
                        bottom: AppPadding.padding6),
                    child: Text(
                        getTitle(),
                        style: AppTextStyles.listHeader),
                ),
                alignment: Alignment(0.0, 0.0),
            )
        );
    }
}

class ContactItemView extends StatelessWidget {

    final Contact contact;
    final bool isSaved;
    final Function(Contact) onItemClick;
    final Function(Contact) onAddContact;
    final Function(Contact) onRemoveContact;

    const ContactItemView({Key key,
        @required this.contact,
        @required this.isSaved,
        this.onItemClick,
        this.onAddContact,
        this.onRemoveContact})
        :super(key: key);

    Widget _getBadge() {
        return Positioned(
            child: ClipRRect(
                borderRadius: BorderRadius.circular(8),
                child: Container(
                    color: AppColors.white,
                    width: 16,
                    height: 16,
                    child: Padding(
                        padding: EdgeInsets.all(3),
                        child: SvgPicture.asset(
                            Assets.ic_bitstamp_small
                        )),
                ),
            ),
            right: 0,
            bottom: 0,
        );
    }

    Widget _getAvatar() {
        return Stack(
            children: <Widget>[
                SizedBox(
                    width: 38,
                    height: 38,
                    child: (contact.avatar != null && contact.avatar.length > 0)
                        ? CircleAvatar(
                        backgroundImage: MemoryImage(contact.avatar))
                        : CircleAvatar(child: Text(contact.initials())),
                ),
                Visibility(
                    visible: isSaved,
                    child: _getBadge(),
                )
            ],
        );
    }

    Widget _getPhone() {
        if (contact.phones != null && contact.phones.length > 0) {
            return Text(contact.phones.toList()[0].value ?? "",
                style: AppTextStyles.listItemContactSubtitle);
        } else {
            return Text("", style: AppTextStyles.listItemContactSubtitle);
        }
    }

    Widget _getInfo() {
        return Expanded(
            child: Padding(
                padding: EdgeInsets.only(
                    left: AppPadding.padding,
                    right: AppPadding.padding),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment
                        .start,
                    children: <Widget>[
                        Text(contact.displayName ?? ""),
                        _getPhone()
                    ],
                ),
            ),
        );
    }

    Widget _getAdd(Function(Contact) onAdd, Function(Contact) onRemove) {
        return Visibility(
            visible: !isSaved,
            replacement: AppIconButton(
                icon: Assets.ic_remove,
                color: AppColors.red,
                onTap: () {
                    print("contact remove from:${contact
                        .displayName} onRemove:$onRemove");
                    if (onRemove != null) {
                        onRemove(contact);
                    }
                }
            ),
            child: AppIconButton(
                icon: Assets.ic_plus,
                color: AppColors.blue,
                onTap: () {
                    print("contact add from:${contact.displayName}");
                    if (onAdd != null) {
                        onAdd(contact);
                    }
                }
            ),
        );
    }

    @override
    Widget build(BuildContext context) {
        return Container(
            child: InkWell(
                onTap: () {
                    if (onItemClick != null) {
                        onItemClick(contact);
                    }
                },
                child: Padding(
                    padding: EdgeInsets.only(
                        left: AppPadding.paddingSmall,
                        top: AppPadding.paddingMini,
                        bottom: AppPadding.paddingMini,
                        right: AppPadding.paddingSmall),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                            _getAvatar(),
                            _getInfo(),
                            _getAdd(onAddContact, onRemoveContact)
                        ],
                    )),
            )
        );
    }

    Contact getContact() {
        return contact;
    }

}



