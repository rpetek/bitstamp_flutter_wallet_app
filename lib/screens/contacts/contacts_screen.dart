import 'package:bitstamp_flutter_wallet_app/colors.dart';
import 'package:bitstamp_flutter_wallet_app/main.dart';
import 'package:bitstamp_flutter_wallet_app/resources/assets.dart';
import 'package:bitstamp_flutter_wallet_app/resources/screens.dart';
import 'package:bitstamp_flutter_wallet_app/resources/strings.dart';
import 'package:bitstamp_flutter_wallet_app/screens/action/action_screen.dart';
import 'package:bitstamp_flutter_wallet_app/screens/contacts/contact_list.dart';
import 'package:bitstamp_flutter_wallet_app/screens/contacts/search_view.dart';
import 'package:bitstamp_flutter_wallet_app/styles.dart';
import 'package:bitstamp_flutter_wallet_app/widgets/toolbar.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

class ContactsScreen extends StatefulWidget {

    final ActionType actionType;

    const ContactsScreen({Key key, @required this.actionType})
        : super(key: key);

    @override
    _ContactsScreenState createState() => _ContactsScreenState();
}

class _ContactsScreenState extends State<ContactsScreen> {
    List<ContactItem> _contacts;
    bool selected = false;
    Box _contactsBox;
    String amount = "";
    final String contactsBoxName = "contactsBox";

    final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey = GlobalKey<
        RefreshIndicatorState>();

    // the GlobalKey is needed to animate the list
    final GlobalKey<AnimatedListState> _listKey = GlobalKey();

    handleSwitch() {
        setState(() {
            selected = !selected;
        });
    }

    @override
    void initState() {
        super.initState();
        _openBox();
        WidgetsBinding.instance
            .addPostFrameCallback((_) =>
            _refreshIndicatorKey.currentState.show());
    }

    Future _openBox() async {
        var dir = await getApplicationDocumentsDirectory();
        Hive.init(dir.path);
        _contactsBox = await Hive.openBox(contactsBoxName);
        return;
    }

    Future<PermissionStatus> _getContactPermission() async {
        PermissionStatus permission = await PermissionHandler()
            .checkPermissionStatus(PermissionGroup.contacts);
        if (permission != PermissionStatus.granted &&
            permission != PermissionStatus.disabled) {
            Map<PermissionGroup,
                PermissionStatus> permissionStatus = await PermissionHandler()
                .requestPermissions([PermissionGroup.contacts]);
            return permissionStatus[PermissionGroup.contacts] ??
                PermissionStatus.unknown;
        } else {
            return permission;
        }
    }

    void _handleInvalidPermissions(PermissionStatus permissionStatus) {
        if (permissionStatus == PermissionStatus.denied) {
            throw new PlatformException(
                code: "PERMISSION_DENIED",
                message: "Access to contacts denied",
                details: null);
        } else if (permissionStatus == PermissionStatus.disabled) {
            throw new PlatformException(
                code: "PERMISSION_DISABLED",
                message: "Contacts not available on device",
                details: null);
        }
    }

    List _getSavedContacts() {
        Map<dynamic, dynamic> raw = _contactsBox.toMap();
        print("box raw:$raw");
        return raw.keys.toList();
    }

    _isContactSaved(String id, List savedContacts) {
        for (dynamic c in savedContacts) {
            if (id == c) {
                print("saved:$c");
                return id == c;
            }
        }
        return false;
    }

    Future<Null> _refresh() {
        return refreshContacts().then((contacts) async {
            _refreshIndicatorKey.currentState.show();
            await Future.delayed(Duration(milliseconds: 250));

            var contactListBitstamp = List<ContactItem>();
            var contactListOther = List<ContactItem>();

            if (contacts != null) {
                for (Contact c in contacts) {
                    if (_isContactSaved(c.identifier, _getSavedContacts())) {
                        contactListBitstamp.add(
                            ContactBitstampItem(contact: c));
                    } else {
                        contactListOther.add(ContactOtherItem(contact: c));
                    }
                }
            }
            var contactItems = List<ContactItem>();
            if (contactListBitstamp.isNotEmpty) {
                contactItems.add(ContactHeaderItem(
                    title: Strings.contacts_header_bitstamp,
                    numOfItems: contactListBitstamp.length));
                contactItems.addAll(contactListBitstamp);
            }
            if (contactListOther.isNotEmpty) {
                contactItems.add(ContactHeaderItem(
                    title: Strings.contacts_header_other,
                    numOfItems: contactListOther.length));
                contactItems.addAll(contactListOther);
            }

            setState(() => _contacts = contactItems);
        });
    }

    Future<List<Contact>> refreshContacts() async {
        PermissionStatus permissionStatus = await _getContactPermission();
        if (permissionStatus == PermissionStatus.granted) {
            var contacts = (await ContactsService.getContacts()).toList();
            return contacts;
        } else {
            _handleInvalidPermissions(permissionStatus);
            return List();
        }
    }

    Widget createContactList(List<ContactItem> contacts) {
        print("rebuild");
        return RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: _refresh,
            child: ContactList(
                listKey: _listKey,
                contacts: contacts,
                onItemClick: (Contact contact) {
                    Navigator.of(context).push(MaterialPageRoute<Null>(
                        settings: RouteSettings(
                            name: AppScreens.screen_action),
                        builder: (BuildContext context) {
                            return ActionScreen(
                                actionType: widget.actionType,
                                contact: contact
                            );
                        }
                    ));
                },
                onAddContact: (Contact contact) {
                    print("contact add:${contact.identifier}");
                    _contactsBox.put(contact.identifier, contact.displayName);
                    _refresh();
                },
                onRemoveContact: (Contact contact) {
                    print("contact remove:${contact.identifier}");
                    _contactsBox.delete(contact.identifier);
                    _refresh();
                },
            )
        );
    }

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            backgroundColor: AppColors.white,
            appBar: Toolbar(
                title: Strings.contacts_title,
                iconAction: Assets.ic_close,
                iconNav: Assets.ic_plus,
                navIconColor: AppColors.blue,
                navIconBckgColor: AppColors.blueLight,
                onNavTap: () {

                },),
            body: SafeArea(
                child: Column(
                    children: <Widget>[
                        Padding(padding: EdgeInsets.only(
                            left: AppPadding.paddingSmall,
                            right: AppPadding.paddingSmall),
                            child: SearchView()
                        ),
                        Expanded(
                            child: createContactList(_contacts),
                        )

                    ],
                )
            )
        );
    }
}