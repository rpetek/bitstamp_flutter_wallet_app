import 'package:bitstamp_flutter_wallet_app/colors.dart';
import 'package:bitstamp_flutter_wallet_app/resources/assets.dart';
import 'package:bitstamp_flutter_wallet_app/resources/strings.dart';
import 'package:bitstamp_flutter_wallet_app/widgets/icon_button.dart';
import 'package:flutter/material.dart';

class SearchView extends StatefulWidget {

    final Function(String text) onTextChange;

    const SearchView({Key key, this.onTextChange}) : super(key: key);

    @override
    _SearchViewState createState() => _SearchViewState();
}

class _SearchViewState extends State<SearchView> {

    bool selected = false;

    handleSwitch() {
        setState(() {
            selected = !selected;
        });
    }

    @override
    Widget build(BuildContext context) {
        return Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
                AppIconButton(icon: Assets.ic_search, color: AppColors.gray),
                Container(
                    child: Flexible(
                        child: TextField(
                            decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: Strings.contacts_search_hint,
                                hintStyle: TextStyle(color: AppColors.gray),
                            ),
                            onChanged: (String text) {
                                if (widget.onTextChange != null) {
                                    widget.onTextChange(text);
                                }
                            },
                        )
                    ),
                )
            ],
        );
    }
}

