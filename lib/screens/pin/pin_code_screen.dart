import 'package:bitstamp_flutter_wallet_app/colors.dart';
import 'package:bitstamp_flutter_wallet_app/main.dart';
import 'package:bitstamp_flutter_wallet_app/models/currency.dart';
import 'package:bitstamp_flutter_wallet_app/resources/assets.dart';
import 'package:bitstamp_flutter_wallet_app/resources/screens.dart';
import 'package:bitstamp_flutter_wallet_app/resources/strings.dart';
import 'package:bitstamp_flutter_wallet_app/screens/response/response_screen.dart';
import 'package:bitstamp_flutter_wallet_app/styles.dart';
import 'package:bitstamp_flutter_wallet_app/widgets/button_elevation.dart';
import 'package:bitstamp_flutter_wallet_app/widgets/keyboard.dart';
import 'package:bitstamp_flutter_wallet_app/widgets/pin_widget.dart';
import 'package:bitstamp_flutter_wallet_app/widgets/toolbar.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/material.dart';

class PinCodeScreen extends StatefulWidget {

    final ActionType actionType;
    final Contact contact;
    final double amount;
    final Currency currency;

    const PinCodeScreen({Key key,
        @required this.actionType,
        @required this.contact,
        @required this.amount,
        @required this.currency})
        : super(key: key);

    @override
    _PinCodeScreenState createState() => _PinCodeScreenState();
}

class _PinCodeScreenState extends State<PinCodeScreen> {

    String pinCode = "";
    int pinLength = 4;

    _addNumToCode(String value) {
        setState(() {
            if (pinCode.length < pinLength) {
                pinCode = pinCode + value;
            }
        });
    }

    _removeNumFromCode() {
        setState(() {
            if (pinCode.length - 1 > 0) {
                pinCode = pinCode.substring(0, pinCode.length - 1);
            } else {
                pinCode = "";
            }
        });
    }

    _clearCode() {
        setState(() {
            pinCode = "";
        });
    }

    _navigateToResponse() {
        Navigator.of(context).push(
            MaterialPageRoute<Null>(
                settings: RouteSettings(name: AppScreens.screen_response),
                builder: (BuildContext context) {
                    return ResponseScreen(
                        actionType: widget.actionType,
                        contact: widget.contact,
                        amount: widget.amount,
                        currency: widget.currency);
                }
            )
        );
    }

    @override
    void initState() {
        super.initState();
    }

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            backgroundColor: AppColors.white,
            appBar: Toolbar(
                title: Strings.pin_code_title,
                iconAction: Assets.ic_close,
                onNavTap: () {

                },),
            body: SafeArea(
                child: Padding(padding: EdgeInsets.only(
                    left: AppPadding.padding,
                    right: AppPadding.padding,
                    top: AppPadding.paddingSmall,
                    bottom: AppPadding.padding),
                    child: Column(
                        children: <Widget>[
                            Expanded(
                                flex: 2,
                                child: Column(
                                    crossAxisAlignment: CrossAxisAlignment
                                        .stretch,
                                    mainAxisAlignment: MainAxisAlignment
                                        .spaceBetween,
                                    children: <Widget>[
                                        Column(
                                            mainAxisAlignment: MainAxisAlignment
                                                .center,
                                            children: <Widget>[
                                                Padding(
                                                    padding: EdgeInsets.only(
                                                        top: AppPadding
                                                            .padding),
                                                    child: Text(
                                                        Strings.pin_code_enter,
                                                        style: AppTextStyles
                                                            .textItemSubtitle,),
                                                ),
                                                Padding(
                                                    padding: EdgeInsets.only(
                                                        top: AppPadding
                                                            .paddingBig),
                                                    child: PinWidget(
                                                        maxPinLength: pinLength,
                                                        pinCode: pinCode),
                                                )
                                            ],
                                        ),
                                        Padding(
                                            padding: EdgeInsets.only(
                                                bottom: AppPadding.padding),
                                            child: ConfirmButton(
                                                pinCode: pinCode,
                                                maxPinLength: pinLength,
                                                onConfirm: () {
                                                    _navigateToResponse();
                                                },),
                                        )
                                    ],
                                ),
                            ),
                            Expanded(
                                flex: 1,
                                child: Keyboard(
                                    showDecimal: false,
                                    onTap: (Object o) {
                                        print("tapped:$o");
                                        if (o is String) {
                                            _addNumToCode(o);
                                        } else if (o is KeyboardActionType) {
                                            if (o ==
                                                KeyboardActionType.BACKSPACE) {
                                                _removeNumFromCode();
                                            }
                                        }
                                    }, onLongTap: (Object o) {
                                    print("tapped long:$o");
                                    if (o is KeyboardActionType) {
                                        if (o ==
                                            KeyboardActionType.BACKSPACE) {
                                            _clearCode();
                                        }
                                    }
                                })
                            )
                        ],
                    ),
                )

            )
        );
    }
}

class ConfirmButton extends StatelessWidget {

    final String pinCode;
    final int maxPinLength;
    final Function onConfirm;

    const ConfirmButton({Key key,
        @required this.pinCode,
        @required this.maxPinLength,
        @required this.onConfirm
    }) :super(key: key);

    _onConfirm(BuildContext context) {
        if (pinCode.length < maxPinLength) {
            final snackBar = SnackBar(
                duration: Duration(milliseconds: 500),
                content: Text(Strings.pin_code_error));
            Scaffold.of(context).showSnackBar(snackBar);
        } else {
            if (this.onConfirm != null) {
                this.onConfirm();
            }
        }
    }

    @override
    Widget build(BuildContext context) {
        return
            ButtonElevation(
                height: 46,
                color: AppColors.green,
                child: FlatButton(
                    shape: StadiumBorder(),
                    color: AppColors.green,
                    onPressed: () {
                        _onConfirm(context);
                    },
                    child: Text(
                        Strings
                            .pin_code_confirm,
                        style: AppTextStyles
                            .textButton),
                )
            );
    }

}
