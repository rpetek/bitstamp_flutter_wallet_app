import 'package:bitstamp_flutter_wallet_app/colors.dart';
import 'package:bitstamp_flutter_wallet_app/main.dart';
import 'package:bitstamp_flutter_wallet_app/models/currency.dart';
import 'package:bitstamp_flutter_wallet_app/resources/assets.dart';
import 'package:bitstamp_flutter_wallet_app/resources/screens.dart';
import 'package:bitstamp_flutter_wallet_app/resources/strings.dart';
import 'package:bitstamp_flutter_wallet_app/screens/action/currency_sheet.dart';
import 'package:bitstamp_flutter_wallet_app/screens/pin/pin_code_screen.dart';
import 'package:bitstamp_flutter_wallet_app/styles.dart';
import 'package:bitstamp_flutter_wallet_app/widgets/button_elevation.dart';
import 'package:bitstamp_flutter_wallet_app/widgets/center_horizontal.dart';
import 'package:bitstamp_flutter_wallet_app/widgets/icon_button.dart';
import 'package:bitstamp_flutter_wallet_app/widgets/keyboard.dart';
import 'package:bitstamp_flutter_wallet_app/widgets/toolbar.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/material.dart';

class ActionScreen extends StatefulWidget {

    final ActionType actionType;
    final Contact contact;

    const ActionScreen({Key key,
        @required this.actionType,
        @required this.contact})
        : super(key: key);

    @override
    _ActionScreenState createState() => _ActionScreenState();
}

class _ActionScreenState extends State<ActionScreen> {

    String amount = "0";
    bool errorVisible = false;
    static List<Currency> currencies = List.unmodifiable(
        [
            Currency(symbol: "\$", code: "USD", name: "US dollar"),
            Currency(symbol: "\€", code: "EUR", name: "Euro"),
            Currency(symbol: "\£", code: "GBP", name: "GB pound")
        ]);
    Currency currency = currencies[0];

    _addToAmount(String value) {
        setState(() {
            if (amount == "0") {
                amount = value;
            } else {
                amount = amount + value;
            }
        });
    }

    _removeFromAmount() {
        setState(() {
            if (amount.length - 1 > 0) {
                amount = amount.substring(0, amount.length - 1);
            } else {
                amount = "0";
            }
        });
    }

    _clearAmount() {
        setState(() {
            amount = "0";
        });
    }

    _changeCurrency(Currency c) {
        setState(() {
            currency = c;
        });
    }

    _showError() {
        setState(() {
            errorVisible = true;
        });
        Future.delayed(Duration(milliseconds: 750), () {
            setState(() {
                errorVisible = false;
            });
        });
    }

    String _getTitle(ActionType actionType) {
        if (actionType == ActionType.REQUEST) {
            return Strings.action_title_request;
        } else if (actionType == ActionType.SEND) {
            return Strings.action_title_send;
        } else if (actionType == ActionType.TRANSFER) {
            return Strings.action_title_transfer;
        } else {
            return "";
        }
    }

    _onConfirm() {
        if (double.parse(amount) >= 50) {
            Navigator.of(context).push(
                MaterialPageRoute<Null>(
                    settings: RouteSettings(name: AppScreens.screen_pin_code),
                    builder: (BuildContext context) {
                        return PinCodeScreen(
                            actionType: ActionType.REQUEST,
                            contact: widget.contact,
                            amount: double.parse(amount),
                            currency: currency);
                    }
                ));
        } else {
            _showError();
        }
    }

    @override
    void initState() {
        super.initState();
    }

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            backgroundColor: AppColors.white,
            appBar: Toolbar(
                title: _getTitle(widget.actionType),
                iconAction: Assets.ic_close,
                onNavTap: () {

                },),
            body: SafeArea(
                child: Padding(padding: EdgeInsets.only(
                    left: AppPadding.padding,
                    right: AppPadding.padding,
                    top: AppPadding.paddingSmall,
                    bottom: AppPadding.padding),
                    child:
                    Column(
                        children: <Widget>[
                            Expanded(
                                flex: 2,
                                child: Column(
                                    crossAxisAlignment: CrossAxisAlignment
                                        .stretch,
                                    mainAxisAlignment: MainAxisAlignment
                                        .spaceBetween,
                                    children: <Widget>[
                                        Wrap(
                                            children: <Widget>[
                                                Header(contact: widget.contact),
                                                Padding(
                                                    padding: EdgeInsets.only(
                                                        top: AppPadding
                                                            .paddingBig),
                                                    child: AmountInput(
                                                        amount: amount,
                                                        currency: currency,
                                                        currencies: currencies,
                                                        errorVisible: errorVisible,
                                                        onCurrencyTap:
                                                            (Currency c) {
                                                            _changeCurrency(c);
                                                        },
                                                    )
                                                ),
                                            ],
                                        ),
                                        Padding(
                                            padding: EdgeInsets.only(
                                                bottom: AppPadding.padding),
                                            child: ButtonElevation(
                                                height: 46,
                                                color: AppColors.green,
                                                child: FlatButton(
                                                    shape: StadiumBorder(),
                                                    color: AppColors.green,
                                                    onPressed: () {
                                                        _onConfirm();
                                                    },
                                                    child: Text(
                                                        Strings.confirm,
                                                        style: AppTextStyles
                                                            .textButton),
                                                )
                                            ),
                                        )
                                    ],
                                ),
                            ),
                            Expanded(
                                flex: 1,
                                child: Keyboard(onTap: (Object o) {
                                    print("tapped:$o");
                                    if (o is String) {
                                        _addToAmount(o);
                                    } else if (o is KeyboardActionType) {
                                        if (o ==
                                            KeyboardActionType.BACKSPACE) {
                                            _removeFromAmount();
                                        }
                                    }
                                }, onLongTap: (Object o) {
                                    print("tapped long:$o");
                                    if (o is KeyboardActionType) {
                                        if (o ==
                                            KeyboardActionType.BACKSPACE) {
                                            _clearAmount();
                                        }
                                    }
                                })
                            )
                        ],
                    ),
                )

            )
        );
    }
}

class Header extends StatelessWidget {

    final Contact contact;

    const Header({Key key,
        @required this.contact})
        :super(key: key);

    @override
    Widget build(BuildContext context) {
        return CenterHorizontal(
            Column(
                children: <Widget>[
                    SizedBox(
                        width: 38,
                        height: 38,
                        child: (contact.avatar != null &&
                            contact.avatar.length > 0)
                            ? CircleAvatar(
                            backgroundImage: MemoryImage(contact.avatar))
                            : CircleAvatar(child: Text(contact.initials())),
                    ),
                    Padding(
                        padding: EdgeInsets.only(top: AppPadding.paddingSmall,
                            bottom: AppPadding.paddingSmall),
                        child: Text(contact.displayName,
                            style: AppTextStyles.textNormal),
                    )
                ],
            )

        );
    }

}

class AmountInput extends StatelessWidget {

    final String amount;
    final Currency currency;
    final List<Currency> currencies;
    final bool errorVisible;
    final Function(Currency c) onCurrencyTap;

    AmountInput({Key key,
        @required this.amount,
        @required this.currency,
        @required this.currencies,
        @required this.errorVisible,
        @required this.onCurrencyTap
    })
        :super(key: key);

    @override
    Widget build(BuildContext context) {
        return Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                        Wrap(
                            crossAxisAlignment: WrapCrossAlignment.center,
                            children: <Widget>[
                                Text(currency.code, style: TextStyle(
                                    color: AppColors.primaryTextColor,
                                    fontSize: 22),),
                                AppIconButton(
                                    height: 30,
                                    width: 30,
                                    radius: 15,
                                    icon: Assets.ic_dropdown,
                                    onTap: () {
                                        showModalBottomSheet(
                                            context: context,
                                            backgroundColor: AppColors.white,
                                            shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadius.only(
                                                    topLeft: Radius.circular(
                                                        15.0),
                                                    topRight: Radius.circular(
                                                        15.0)),
                                            ),
                                            builder: (context) =>
                                                CurrencySheet(
                                                    currencies: currencies,
                                                    onTap: (Currency currency) {
                                                        if (this
                                                            .onCurrencyTap !=
                                                            null) {
                                                            this.onCurrencyTap(
                                                                currency);
                                                            Navigator.of(
                                                                context).pop();
                                                        }
                                                    })
                                        );
                                    },
                                )
                            ],
                        ),
                        Wrap(
                            children: <Widget>[
                                Text(currency.symbol, style: TextStyle(
                                    color: AppColors.primaryTextColor,
                                    fontSize: 22)
                                ),
                                Text(amount, style: TextStyle(
                                    color: AppColors.primaryTextColor,
                                    fontSize: 22)
                                )
                            ],
                        )

                    ],
                ),
                SizedBox(height: AppPadding.padding),
                Divider(height: 1, color: AppColors.gray),
                SizedBox(height: AppPadding.paddingMini),
                AnimatedOpacity(
                    // If the widget is visible, animate to 0.0 (invisible).
                    // If the widget is hidden, animate to 1.0 (fully visible).
                    opacity: errorVisible ? 1.0 : 0.0,
                    duration: Duration(milliseconds: 250),
                    // The green box must be a child of the AnimatedOpacity widget.
                    child: Text("Amount must be larger than 50\$",
                        style: AppTextStyles.textSmallError),
                )
            ]);
    }

}