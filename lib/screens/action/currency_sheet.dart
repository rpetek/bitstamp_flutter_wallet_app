import 'package:bitstamp_flutter_wallet_app/colors.dart';
import 'package:bitstamp_flutter_wallet_app/models/currency.dart';
import 'package:bitstamp_flutter_wallet_app/styles.dart';
import 'package:flutter/material.dart';


class CurrencySheet extends StatelessWidget {

    final List<Currency> currencies;
    final Function(Currency currency) onTap;

    const CurrencySheet({
        @required this.currencies,
        this.onTap}) :super();

    @override
    Widget build(BuildContext context) {
        return Container(
            child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                    CurrencySheetRow(
                        currency: currencies[0],
                        onTap: (Currency currency) {
                            if (onTap != null) {
                                onTap(currency);
                            }
                        }
                    ),
                    CurrencySheetRowDivider(),
                    CurrencySheetRow(
                        currency: currencies[1],
                        onTap: (Currency currency) {
                            if (onTap != null) {
                                onTap(currency);
                            }
                        }
                    ),
                    CurrencySheetRowDivider(),
                    CurrencySheetRow(
                        currency: currencies[2],
                        onTap: (Currency currency) {
                            if (onTap != null) {
                                onTap(currency);
                            }
                        }
                    )
                ]
            )
        );
    }
}

class CurrencySheetRow extends StatelessWidget {
    final Currency currency;
    final Function(Currency currency) onTap;

    const CurrencySheetRow({
        @required this.currency,
        this.onTap}) :super();

    @override
    Widget build(BuildContext context) {
        return Container(
            height: 60,
            child: InkWell(
                onTap: () {
                    if (this.onTap != null) {
                        this.onTap(currency);
                    }
                },
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                        Text(currency.code,
                            style: AppTextStyles.textItemSmallTitle)
                    ],
                )
            )
        );
    }
}

class CurrencySheetRowDivider extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return Container(
            height: 1,
            color: AppColors.grayLight,
        );
    }
}