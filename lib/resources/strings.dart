class Strings {

    static const String contacts_title = "Select contact";
    static const String contacts_search_hint = "Search for contacts...";
    static const String contacts_header_bitstamp = "Bitstamp contacts";
    static const String contacts_header_other = "Other contacts";
    static const String action_title_send = "Send to";
    static const String action_title_request = "Request from";
    static const String action_title_transfer = "Transfer between";
    static const String confirm = "Confirm";
    static const String request_title = "Request";
    static const String request_subtitle = "Request funds from friends";
    static const String send_title = "Send";
    static const String send_subtitle = "Send funds to friends";
    static const String transfer_title = "Transfer";
    static const String transfer_subtitle = "Transfer funds between friends";
    static const String pin_code_title = "PIN Code";
    static const String pin_code_enter = "Enter your 4-digit PIN code";
    static const String pin_code_confirm = "Confirm PIN code";
    static const String pin_code_error = "Please enter a 4-digit PIN code";
    static const String response_success_title = "Success!";
    static const String response_success_subtitle = "Transaction completed";
    static const String response_success_action = "OK";

}