class Assets {

    static const ic_action_request = "assets/icons/action_request.svg";
    static const ic_action_send = "assets/icons/action_send.svg";
    static const ic_action_transfer = "assets/icons/action_transfer.svg";
    static const ic_card = "assets/icons/card.svg";
    static const ic_card_lg = "assets/icons/card_lg.svg";
    static const ic_card_sm = "assets/icons/card_sm.svg";
    static const ic_close = "assets/icons/close.svg";
    static const ic_dashboard = "assets/icons/dashboard.svg";
    static const ic_deposit = "assets/icons/deposit.svg";
    static const ic_lock = "assets/icons/lock.svg";
    static const ic_plus = "assets/icons/plus.svg";
    static const ic_remove = "assets/icons/remove.svg";
    static const ic_reset = "assets/icons/reset.svg";
    static const ic_send = "assets/icons/send.svg";
    static const ic_settings = "assets/icons/settings.svg";
    static const ic_wallet = "assets/icons/wallet.svg";
    static const ic_search = "assets/icons/search.svg";
    static const ic_bitstamp_small = "assets/icons/bitstamp_logo_small.svg";
    static const ic_backspace = "assets/icons/backspace.svg";
    static const ic_dropdown = "assets/icons/dropdown.svg";
    static const ic_success = "assets/icons/success.svg";

}