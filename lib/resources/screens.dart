

class AppScreens {

    static const String screen_home = "/";
    static const String screen_card_settings = "card-settings-screen";
    static const String screen_contacts = "contact-list-screen";
    static const String screen_action = "action-screen";
    static const String screen_pin_code = "pin-code-screen";
    static const String screen_response = "response-screen";

}